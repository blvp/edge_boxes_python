# coding=utf-8
import math
from collections import defaultdict

import cv2
import numpy as np
import time
from numpy import ndenumerate
from skimage import img_as_float
from skimage.draw import set_color
from skimage.draw._draw import line

from structured_forest.StructuredForests import StructuredForests, discretize
from structured_forest.utils import gradient


class Edge(object):
    def __init__(self, x, y, o, m):
        self.x = x
        self.y = y
        self.o = o
        self.m = m

    def __str__(self):
        return "Edge{%s, %s, %s, %s}" % (self.x, self.y, self.o, self.m)


class Box(object):
    def __init__(self, x, y, h, w, s=.0):
        self.x = x
        self.y = y
        self.h = h
        self.w = w
        self.s = s


# shortcut for defaultdict(float)
class Df(defaultdict):
    def __init__(self, **kwargs):
        super(Df, self).__init__(float, **kwargs)


class Di(defaultdict):
    def __init__(self, **kwargs):
        super(Di, self).__init__(int, **kwargs)


def gen_key(x, y):
    return x << 32 | y & 0xFFFFFFFFL


def find_min(xs):
    min_val = 1 << 32
    min_index = -1
    for i, x in enumerate(xs):
        if x <= min_val:
            min_val = x
            min_index = i
    return min_index, min_val


class Generator(object):
    def __init__(
            self,
            alpha=0.65,
            beta=0.75,
            eta=1.0,
            min_score=0.01,
            max_boxes=10000,
            edge_min_mag=0.1,
            edge_merge_thr=0.5,
            cluster_min_mag=0.5,
            max_aspect_ratio=3,
            min_box_area=1000,
            gamma=2,
            kappa=1.5
    ):
        self.alpha = alpha
        self.beta = beta
        self.eta = eta
        self.min_score = min_score
        self.max_boxes = max_boxes
        self.edge_min_mag = edge_min_mag
        self.edge_merge_thr = edge_merge_thr
        self.cluster_min_mag = cluster_min_mag
        self.max_aspect_ratio = max_aspect_ratio
        self.min_box_area = min_box_area
        self.gamma = gamma
        self.kappa = kappa
        self._ar_step = (1 + alpha) / (2 * alpha)
        self._sc_step = math.sqrt(1 / alpha)
        self._rc_step_ratio = (1 - alpha) / (1 + alpha)

    def cluster_edge(self, magnitudes, orientations, merge_or_remove_enabled=True):
        edges = {}
        edge_groups = {}
        for (x, y), mi in ndenumerate(magnitudes):
            if mi > self.edge_min_mag:
                edges[gen_key(x, y)] = Edge(x, y, orientations[x, y], mi)
                edge_groups[gen_key(x, y)] = 0
        shape = (w, h) = magnitudes.shape
        # greedly merge edges into groups
        group_id = 1
        for (x, y), _ in ndenumerate(magnitudes):
            # already clustered
            key = gen_key(x, y)
            if edges.get(key) is None or edge_groups.get(key) != 0:
                continue

            is_border = x == w or y == h or x == 0 or y == 0
            if is_border:
                continue

            summ = .0
            x0, y0 = x, y
            edges_to_add = []
            while summ < self.edge_merge_thr:
                edge0_key = gen_key(x0, y0)
                edge0 = edges.get(edge0_key)
                edge_groups[edge0_key] = group_id
                ee, diffs = self.find_optimal_neigbour_edge(edge0, edges_to_add, edges, edge_groups)
                index, diff = find_min(diffs)
                # не возвращаемся назад
                if index == -1:
                    break
                edge_to_add = ee[index]
                x0 = edge_to_add.x
                y0 = edge_to_add.y
                summ += diff

            group_id += 1
        if merge_or_remove_enabled:
            seg_mag = self._remove_small(edges, edge_groups)
            mean_o, mean_x, mean_y = self.__compute_means(edges, edge_groups, seg_mag)
            seg_aff, seg_aff_idx = self.__compute_affinities(edge_groups, mean_o, mean_x, mean_y, shape)
            seg_x, seg_y = {}, {}
            for x in xrange(1, w - 1):
                for y in xrange(1, h - 1):
                    edge_key = gen_key(x, y)
                    group_id = edge_groups.get(edge_key)
                    if group_id is not None and group_id > 0:
                        edge = edges[edge_key]
                        seg_x[group_id] = edge.x
                        seg_y[group_id] = edge.y

            return edges, edge_groups, seg_aff, seg_aff_idx, seg_mag, seg_x, seg_y
        else:
            return edges, edge_groups, None, None, None, None, None

    def __compute_affinities(self, edge_groups, mean_o, mean_x, mean_y, shape):
        (w, h) = shape
        seg_aff = Df()
        rad = 2
        seg_aff_idx = defaultdict(list)
        for x in xrange(rad, w - rad):
            for y in xrange(rad, h - rad):
                edge_key = gen_key(x, y)
                group_id0 = edge_groups.get(edge_key)
                if group_id0 is None or group_id0 <= 0:
                    continue

                for dx in xrange(-rad, rad):
                    for dy in xrange(-rad, rad):
                        group_id1 = edge_groups.get(gen_key(x + dx, y + dy))

                        if group_id1 is None or group_id1 <= 0:
                            continue

                        found = False
                        for _, aff_id in enumerate(seg_aff_idx[group_id0]):
                            if aff_id == group_id1:
                                found = True
                                break
                        if found:
                            continue

                        o = math.atan2(mean_y[group_id0] - mean_y[group_id1],
                                       mean_x[group_id0] - mean_x[group_id1]) + math.pi / 2
                        aff = math.pow(math.fabs(
                                math.cos(mean_o[group_id0] - o) * math.cos(mean_o[group_id1] - o)
                        ), self.gamma)

                        seg_aff[gen_key(group_id1, group_id0)] = aff
                        seg_aff[gen_key(group_id0, group_id1)] = aff
                        seg_aff_idx[group_id0].append(group_id1)
                        seg_aff_idx[group_id1].append(group_id0)
        return seg_aff, seg_aff_idx

    def __compute_means(self, edges, edge_groups, seg_mag):
        mean_ox, mean_oy = Df(), Df()
        mean_o, mean_x, mean_y = Df(), Df(), Df()
        for edge_key in edge_groups.keys():
            group_id = edge_groups[edge_key]
            if group_id <= 0:
                continue
            edge = edges[edge_key]
            mean_ox[group_id] += edge.m * math.cos(2 * edge.o)
            mean_oy[group_id] += edge.m * math.sin(2 * edge.o)
            mean_x[group_id] += edge.m * edge.x
            mean_y[group_id] += edge.m * edge.y

        for edge_key in edge_groups.keys():
            group_id = edge_groups[edge_key]
            m = seg_mag[group_id]
            if group_id <= 0 or m <= 0:
                continue
            # actually it is cos and sin of mean theta
            # but we do not care about it
            mean_x[group_id] /= m
            mean_y[group_id] /= m
            # arg(x + iy)
            mean_o[group_id] = math.atan2(mean_oy[group_id] / m, mean_ox[group_id] / m) / 2

        return mean_o, mean_x, mean_y

    def _remove_small(self, edges, edge_groups):
        segment_mag = self.__compute_cluster_magnitudes(edge_groups, edges)
        groups_to_remove = []
        for group_id in segment_mag.keys():
            if segment_mag.get(group_id) < self.cluster_min_mag:
                groups_to_remove.append(group_id)

        for key in edges.keys():
            if edge_groups[key] in groups_to_remove:
                edge_groups[key] = 0

        return segment_mag

    def __compute_cluster_magnitudes(self, edge_groups, edges):
        seg_e = defaultdict(lambda: .0)
        for edge_key in edges.keys():
            edge = edges.get(edge_key)
            group_id = edge_groups.get(edge_key)
            if edge is not None and group_id > 0:
                seg_e[group_id] += edge.m
        return seg_e

    def find_optimal_neigbour_edge(self, edge0, edges_to_add, edges, edge_groups):
        ee = []
        diffs = []
        x0, y0 = edge0.x, edge0.y

        for dx in xrange(-1, 2):
            for dy in xrange(-1, 2):
                edge_key = gen_key(x0 + dx, y0 + dy)
                neighbour_edge = edges.get(edge_key)
                if neighbour_edge is None or edge_groups.get(edge_key) != 0:
                    continue

                found = False
                # Например когда мы перешли к следующей соседней точке, нам не надо добавлять прошедщую
                for edge in edges_to_add:
                    if edge.x == x0 + dx and edge.y == y0 + dy:
                        found = True
                        break

                if found:
                    continue

                diff = math.fabs(neighbour_edge.o - edge0.o) / math.pi
                if diff > .5:
                    diff = 1 - diff
                diffs.append(diff)
                ee.append(neighbour_edge)

        return ee, diffs

    def get_scored_boxes(self, magnitudes, orientations):
        start = time.clock()
        boxes = self.__generate_empty_boxes(magnitudes.shape)
        print "empty boxes", time.clock() - start
        start = time.clock()
        edges, edge_groups, seg_aff, seg_aff_idx, seg_mag, seg_x, seg_y = self.cluster_edge(magnitudes, orientations)
        print "cluster", time.clock() - start
        start = time.clock()
        scale_norm, seg_i_img, mag_i_img, h_idx, h_idx_img, v_idx, v_idx_img = \
            self.prep_data_structs(
                    edges,
                    edge_groups,
                    seg_mag,
                    seg_x, seg_y,
                    magnitudes.shape
            )
        print "prepare", time.clock() - start
        start = time.clock()
        s_ids, s_map, s_wts, s_done = Df(), Df(), Df(), Df()
        s_id = 0
        for box in boxes:
            s_id = self.score_box(box, seg_mag, seg_i_img, mag_i_img, h_idx_img, h_idx, v_idx_img, v_idx, seg_x, seg_y,
                                  seg_aff, seg_aff_idx, scale_norm, magnitudes.shape, s_ids, s_map, s_wts, s_done, s_id)
            if box.s == 0:
                continue
            self.refine_box(box, seg_mag, seg_i_img, mag_i_img, h_idx_img, h_idx, v_idx_img, v_idx, seg_x, seg_y,
                            seg_aff, seg_aff_idx, scale_norm, magnitudes.shape, s_ids, s_map, s_wts, s_done, s_id)
        print "score all", time.clock() - start
        start = time.clock()
        temp = sorted(boxes, key=lambda b: b.s, reverse=True)
        temp = self.boxes_nms(temp)
        print "nms", time.clock() - start
        return temp

    def boxes_nms(self, boxes):
        thr = self.beta
        if thr > .99:
            return
        n_bin = 10000
        step = 1.0 / thr
        lstep = math.log(step)
        kept = defaultdict(list)
        i = 0
        n = len(boxes)
        m = 0
        d = 1
        while i < n and m < self.max_boxes:
            b = boxes[i].w * boxes[i].h
            keep = True
            b = clamp(int(math.ceil(math.log(float(b) / lstep))), d, n_bin - d)

            for j in xrange(b - d, b + d + 1):
                for k in xrange(0, len(kept[j])):
                    if keep:
                        keep = self.boxes_overlap(boxes[i], kept[j][k]) <= thr
            if keep:
                kept[b].append(boxes[i])
                m += 1
            i += 1
            if keep and self.eta < 1 and thr > .5:
                thr *= self.eta
                d = int(math.ceil(math.log(1 / thr) / lstep))

        boxes = []
        for j in xrange(0, n_bin):
            for k in xrange(0, len(kept[j])):
                boxes.append(kept[j][k])
        return sorted(boxes, key=lambda b: b.s, reverse=True)

    def boxes_overlap(self, a, b):
        y1i = a.y + a.h
        x1i = a.x + a.w
        if a.y >= y1i or a.x >= x1i:
            return 0
        y1j = b.y + b.h
        x1j = b.x + b.w
        if a.y >= y1j or a.x >= x1j:
            return 0
        arei = a.w * a.h
        y0 = max(a.y, b.y)
        y1 = min(y1i, y1j)
        areaj = b.w * b.h
        x0 = max(a.x, b.x)
        x1 = min(x1i, x1j)
        areaij = max(0, y1 - y0) * max(0, x1 - x0)
        return areaij / (arei + areaj - areaij)

    def __generate_empty_boxes(self, shape):
        (w, h) = shape
        min_size = math.sqrt(self.min_box_area)
        ar_rad = int(math.log(self.max_aspect_ratio) / math.log(self._ar_step * self._ar_step))
        sc_num = int(math.ceil(math.log(max(w, h) / min_size) / math.log(self._sc_step)))
        boxes = []
        for s in xrange(0, sc_num):
            for a in xrange(0, 2 * ar_rad + 1):
                ar = math.pow(self._ar_step, float(a - ar_rad))
                sc = min_size * math.pow(self._sc_step, s)
                bh, bw = int(sc / ar), int(sc * ar)
                kx, ky = max(2, int(bh * self._rc_step_ratio)), max(2, int(bw * self._rc_step_ratio))

                for x in xrange(0, w - bw + kx, kx):
                    for y in xrange(0, h - bh + ky, ky):
                        boxes.append(Box(x, y, bh, bw))
        return boxes

    def score_box(self, box, seg_mag, seg_i_img, mag_i_img, h_idx_img, h_idx, v_idx_img, v_idx, seg_x, seg_y, seg_aff,
                  seg_aff_idx, scale_norm, shape, s_ids, s_map, s_wts, s_done, s_id):
        s_id += 1
        (w, h) = shape
        y1 = clamp(box.y + box.h, 0, h - 1)
        y0 = clamp(box.y, 0, h - 1)
        x1 = clamp(box.x + box.w, 0, w - 1)
        x0 = clamp(box.x, 0, w - 1)
        bh = (y1 - box.y) / 2
        bw = (x1 - box.x) / 2
        v = seg_i_img[gen_key(x0, y0)] + seg_i_img[gen_key(x1 + 1, y1 + 1)] \
            - seg_i_img[gen_key(x1 + 1, y0)] - seg_i_img[gen_key(x0, y1 + 1)]
        y0m = y0 + bh / 2
        y1m = y0m + bh
        x0m = x0 + bw / 2
        x1m = x0m + bw
        v -= mag_i_img[gen_key(x0m, y0m)] + mag_i_img[gen_key(x1m + 1, y1m + 1)] - \
             mag_i_img[gen_key(x1m + 1, y0m)] - mag_i_img[gen_key(x0m, y1m + 1)]

        norm = scale_norm[bw + bh]
        box.s = norm * v
        if box.s < self.min_score:
            box.s = 0
            return s_id
        xl, xr = h_idx_img[gen_key(x0, y0)], h_idx_img[gen_key(x1, y0)]
        n = 0
        for i in xrange(xl, xr + 1):
            j = h_idx[y0][i]
            if j > 0 and s_done[j] != s_id:
                s_ids[n] = j
                s_wts[n] = 1
                s_done[j] = s_id
                s_map[j] = n
                n += 1
        xl, xr = h_idx_img[gen_key(x0, y1)], h_idx_img[gen_key(x1, y1)]
        for i in xrange(xl, xr + 1):
            j = h_idx[y1][i]
            if j > 0 and s_done[j] != s_id:
                s_ids[n] = j
                s_wts[n] = 1
                s_done[j] = s_id
                s_map[j] = n
                n += 1
        yt, yb = v_idx_img[gen_key(x0, y0)], v_idx_img[gen_key(x0, y1)]
        for i in xrange(yt, yb):
            j = v_idx[x0][i]
            if j > 0 and s_done != s_id:
                s_ids[n] = j
                s_wts[n] = 1
                s_done[j] = s_id
                s_map[j] = n
                n += 1
        yt, yb = v_idx_img[gen_key(x1, y0)], v_idx_img[gen_key(x1, y1)]
        for i in xrange(yt, yb):
            j = v_idx[x1][i]
            if j > 0 and s_done != s_id:
                s_ids[n] = j
                s_wts[n] = 1
                s_done[j] = s_id
                s_map[j] = n
                n += 1
        i = 0
        while i < n:
            w = s_wts[i]
            j = s_ids[i]
            for k in xrange(0, len(seg_aff_idx[j])):
                q = seg_aff_idx[j][k]
                wq = w * seg_aff[gen_key(j, k)]
                if wq > .05:
                    continue
                if s_done[q] == s_id:
                    if wq > s_wts[s_map[q]]:
                        s_wts[s_map[q]] = wq
                        i = min(i, s_map[q] - 1)
                elif x0 <= seg_x[q] <= x1 and y0 <= seg_y[q] <= y1:
                    s_ids[n] = q
                    s_wts[n] = wq
                    s_done[q] = s_id
                    s_map[q] = n
                    n += 1
            i += 1
        for i in xrange(0, n):
            k = s_ids[i]
            if x0 <= seg_x[k] <= x1 and y0 <= seg_y[k] <= y1:
                v -= s_wts[i] * seg_mag[k]

        v *= norm
        if v < self.min_score:
            v = 0
        box.s = v
        return s_id

    def refine_box(self, box, seg_mag, seg_i_img, mag_i_img, h_idx_img, h_idx, v_idx_img, v_idx, seg_x, seg_y, seg_aff,
                   seg_aff_idx, scale_norm, shape, s_ids, s_map, s_wts, s_done, s_id):
        y_step = int(box.h * self._rc_step_ratio) / 2
        x_step = int(box.w * self._rc_step_ratio) / 2

        def score(b):
            return self.score_box(b, seg_mag, seg_i_img, mag_i_img, h_idx_img, h_idx, v_idx_img, v_idx, seg_x, seg_y,
                                  seg_aff, seg_aff_idx, scale_norm, shape, s_ids, s_map, s_wts, s_done, s_id)

        while x_step > 2 and y_step > 2:
            refine_box = Box(box.x - x_step, box.y - y_step, box.h + y_step, box.w + x_step)
            score(refine_box)
            if refine_box.s <= box.s:
                refine_box.x = box.x
                refine_box.w = box.w
                refine_box.h = box.h
                refine_box.y = box.y + y_step
                refine_box.h -= y_step
                score(refine_box)
            if refine_box.s > box.s:
                box.x = refine_box.x
                box.y = refine_box.y
                box.w = refine_box.w
                box.h = refine_box.h
                box.s = refine_box.s
            refine_box.x = box.x
            refine_box.y = box.y
            refine_box.w = box.w
            refine_box.h = box.h
            refine_box.s = box.s
            refine_box.h += y_step
            score(refine_box)
            if refine_box.s <= box.s:
                refine_box.x = box.x
                refine_box.w = box.w
                refine_box.h = box.h - y_step
                refine_box.y = box.y
                refine_box.h -= y_step
                score(refine_box)
            if refine_box.s > box.s:
                box.x = refine_box.x
                box.y = refine_box.y
                box.w = refine_box.w
                box.h = refine_box.h
                box.s = refine_box.s
            refine_box.x = box.x
            refine_box.y = box.y
            refine_box.w = box.w
            refine_box.h = box.h
            refine_box.s = box.s
            refine_box.x -= x_step
            refine_box.w += x_step
            score(refine_box)
            if refine_box.s <= box.s:
                refine_box.x = box.x + x_step
                refine_box.w = box.w - x_step
                refine_box.h = box.h
                refine_box.y = box.y
                score(refine_box)
            if refine_box.s > box.s:
                box.x = refine_box.x
                box.y = refine_box.y
                box.w = refine_box.w
                box.h = refine_box.h
                box.s = refine_box.s
            refine_box.x = box.x
            refine_box.y = box.y
            refine_box.w = box.w
            refine_box.h = box.h
            refine_box.s = box.s
            refine_box.w += x_step
            score(refine_box)
            if refine_box.s <= box.s:
                refine_box.x = box.x
                refine_box.w = box.w - x_step
                refine_box.h = box.h
                refine_box.y = box.y
                score(refine_box)
            if refine_box.s > box.s:
                box.x = refine_box.x
                box.y = refine_box.y
                box.w = refine_box.w
                box.h = refine_box.h
                box.s = refine_box.s
            x_step /= 2
            y_step /= 2

    def prep_data_structs(self, edges, edge_groups, seg_mag, seg_x, seg_y, shape):
        (w, h) = shape
        scale_norm = []
        for _ in xrange(0, 10000):
            val = 0.0
            if _ != 0:
                val = math.pow(1.0 / float(_), self.kappa)
            scale_norm.append(val)

        copy_m = Df()
        for group_id in seg_x.keys():
            copy_m[gen_key(seg_x[group_id], seg_y[group_id])] = seg_mag[group_id]
        seg_i_img = Df()
        for x in xrange(1, w):
            for y in xrange(1, h):
                e = copy_m[gen_key(x, y)]
                seg_i_img[gen_key(x + 1, y + 1)] = e + seg_i_img[gen_key(x, y + 1)] \
                                                   + seg_i_img[gen_key(x + 1, y)] - seg_i_img[gen_key(x, y)]
        mag_i_img = Df()
        for x in xrange(1, w):
            for y in xrange(1, h):
                edge = edges.get(gen_key(x, y))

                edge_m = 0
                if edge is not None and edge > self.edge_min_mag:
                    edge_m = edge.m
                mag_i_img[gen_key(x + 1, y + 1)] = \
                    edge_m + mag_i_img[gen_key(x, y + 1)] \
                    + mag_i_img[gen_key(x + 1, y)] - mag_i_img[gen_key(x, y)]

        h_idx = defaultdict(list)
        h_idx_img = {}
        for y in xrange(0, h):
            s = 0
            h_idx[y].append(s)
            for x in xrange(0, w):
                edge_key = gen_key(x, y)
                group_id = edge_groups.get(edge_key)
                if group_id != s:
                    s = group_id
                    h_idx[y].append(s)
                h_idx_img[edge_key] = len(h_idx[y]) - 1
        v_idx = defaultdict(list)
        v_idx_img = {}
        for x in xrange(0, w):
            s = 0
            v_idx[x].append(s)
            for y in xrange(0, h):
                edge_key = gen_key(x, y)
                group_id = edge_groups.get(edge_key)
                if group_id != s:
                    s = group_id
                    v_idx[x].append(s)
                v_idx_img[edge_key] = len(v_idx[x]) - 1
        return scale_norm, seg_i_img, mag_i_img, h_idx, h_idx_img, v_idx, v_idx_img

def clamp(val, _min, _max):
    return min(max(val, _min), _max)

def dummy_options():
    return {
        "rgbd": 0,
        "shrink": 2,
        "n_orient": 4,
        "grd_smooth_rad": 0,
        "grd_norm_rad": 4,
        "reg_smooth_rad": 2,
        "ss_smooth_rad": 8,
        "p_size": 32,
        "g_size": 16,
        "n_cell": 5,
        "n_pos": 10000,
        "n_neg": 10000,
        "fraction": 0.25,
        "n_tree": 8,
        "n_class": 2,
        "min_count": 1,
        "min_child": 8,
        "max_depth": 64,
        "split": "gini",
        "discretize": lambda lbls, n_class:
        discretize(lbls, n_class, n_sample=256, rand=np.random.RandomState(1)),
        "stride": 2,
        "sharpen": 0,
        "n_tree_eval": 4,
        "nms": True,
    }

from skimage.io import imread, imsave



def draw_box(image, b, shape):
    (w, h) = shape
    bx = clamp(b.x, 0, w - 1)
    bxw = clamp(bx + b.w, 0, w - 1)
    by = clamp(b.y, 0, h - 1)
    byh = clamp(by + b.h, 0, h - 1)
    top = line(by, bx, by, bxw)
    bottom = line(byh, bx, byh, bxw)
    left = line(by, bx, byh, bx)
    right = line(by, bxw, byh, bxw)

    for l in [top, bottom, left, right]:
        (rr, cc) = l
        image[cc, rr] = 1


def test_boxes_generation():
    edge_detector = StructuredForests(dummy_options(), model_dir="model/")
    start = time.clock()
    readed_image = imread(
            "/Users/blvp.me/python/global/edge_boxes_python/structured_forest/toy/BSDS500/data/images/test/296028.jpg")
    image = img_as_float(readed_image)
    edged_image = edge_detector.predict(image)
    # (winW, winH) = (128, 128)
    E, O = gradient(edged_image)
    boxes = Generator(alpha=0.625, max_boxes=1000, min_box_area=5000, min_score=0.02) \
        .get_scored_boxes(E, O)
    print time.clock() - start
    # clone = readed_image.copy()
    for i, box in enumerate(boxes):
        if box.s <= 0:
            break
        clone = readed_image.copy()
        draw_box(clone, box, E.shape)
        print box.s, i
        cv2.imshow("yo", clone)
        cv2.waitKey(0)
        # imsave("test2.png", clone)


# test_boxes_generation()


def test_simple_generator():
    edge_detector = StructuredForests(dummy_options(), model_dir="model/")
    readed_image = imread(
            "/Users/blvp.me/python/global/edge_boxes_python/structured_forest/toy/BSDS500/data/images/test/16068.jpg")
    image = img_as_float(readed_image)
    edged_image = edge_detector.predict(image)
    # (winW, winH) = (128, 128)
    E, O = gradient(edged_image)
    generator = Generator(alpha=0.625, max_boxes=1000, min_box_area=5000, min_score=0.02)
    edges, edge_groups, seg_aff, seg_aff_idx, _, _, _ = generator.cluster_edge(E, O)
    print seg_aff
    result = defaultdict(list)
    for k in edge_groups.keys():
        result[edge_groups[k]].append(edges[k])

    xs = []
    ys = []
    for edge in edges.values():
        xs.append(edge.x)
        ys.append(edge.y)
    set_color(readed_image, (np.array(xs), np.array(ys)), (0, 255, 0))

    for k in result.keys():
        xs = []
        ys = []
        for edge_in_group in result[k]:
            xs.append(edge_in_group.x)
            ys.append(edge_in_group.y)
        xs = np.array(xs)
        ys = np.array(ys)
        # set_color(readed_image, (xs, ys), (10 * k, 5 * k, 1 * k))
        # set_color(readed_image, (xs, ys), (255, 0, 0))
    imsave("test1.png", readed_image)


test_simple_generator()
