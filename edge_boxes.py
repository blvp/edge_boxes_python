import time
import cv2
import math
import numpy as np
from skimage import img_as_float
from skimage.io import imread
from structured_forest.StructuredForests import StructuredForests, discretize
from structured_forest.utils import gradient
from util.utils import pyramid
from util.utils import sliding_window


class Edge(object):
    def __init__(self, x, y, orientation, magnitude):
        self.x = x
        self.y = y
        self.orientation = orientation
        self.magnitude = magnitude

    def get_key_point(self):
        return cv2.KeyPoint(self.x, self.y, 2)

    def get_point(self):
        return self.x, self.y

    def __str__(self):
        return " ".join(map(lambda t: str(t), [self.x, self.y, self.magnitude, self.orientation]))


def dummy_options():
    return {
        "rgbd": 0,
        "shrink": 2,
        "n_orient": 4,
        "grd_smooth_rad": 0,
        "grd_norm_rad": 4,
        "reg_smooth_rad": 2,
        "ss_smooth_rad": 8,
        "p_size": 32,
        "g_size": 16,
        "n_cell": 5,

        "n_pos": 10000,
        "n_neg": 10000,
        "fraction": 0.25,
        "n_tree": 8,
        "n_class": 2,
        "min_count": 1,
        "min_child": 8,
        "max_depth": 64,
        "split": "gini",
        "discretize": lambda lbls, n_class:
        discretize(lbls, n_class, n_sample=256, rand=np.random.RandomState(1)),
        "stride": 2,
        "sharpen": 2,
        "n_tree_eval": 4,
        "nms": True,
    }


def neighbours(edged_image):
    return []


if __name__ == '__main__':
    edge_detector = StructuredForests(dummy_options())

    image = img_as_float(imread("/home/blvp/Downloads/image-banner2.jpg"))
    edged_image = edge_detector.predict(image)
    # (winW, winH) = (128, 128)
    magnitudes, orientations = gradient(edged_image)
    # for resized in pyramid(edged_image, scale=1.5):
    #     # loop over the sliding window for each layer of the pyramid
    #     for (winX, winY, window) in sliding_window(resized, stepSize=32, windowSize=(winW, winH)):
    #         # if the window does not meet our desired window size, ignore it
    #         if window.shape[0] != winH or window.shape[1] != winW:
    #             continue
    #         edges = []
    #         for (_x, _y), value in np.ndenumerate(window):
    #             mx = _x + winX
    #             my = _y + winY
    #             mp = magnitudes[my, mx]
    #             # we decide that pixel with magnitude > .1 is the edge
    #             theta_p = orientations[my, mx]
    #             edges.append(Edge(mx, my, theta_p, mp))
    #         time.sleep(0.025)
    #         clone = resized.copy()
    #         cv2.drawKeypoints(clone, map(lambda e: e.get_key_point(), edges), clone)
    #         cv2.imshow("Window", clone)
    #         cv2.waitKey(1)

    
    for (x, y), _ in np.ndenumerate(edged_image):
        mp = magnitudes[x, y]
        # we decide that pixel with magnitude > .1 is the edge
        if mp > .1:
            connected = neighbours(edged_image)
            if len(connected) == 0:
                print "yay"

    copy = image.copy()

    # image
    cv2.imshow("win", copy)
    cv2.waitKey(0)


def affinity(s1, s2, gamma=2):
    import math
    cos = sum([i * j for (i, j) in zip(s1, s2)])
    theta_ij = math.acos(cos)
    return math.fabs(math.cos(s1.theta - theta_ij) * math.cos(s2.theta - theta_ij)) ** gamma
