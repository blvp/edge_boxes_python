import math
import numpy as np


class Box(object):
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h


class EdgeBoxGenerator(object):
    def __init__(self):
        self.__segment_count = 0

    def generate(self, magnitudes, orientations):
        """
        Generates boxes of :class Box with a given edge response and orientations
        :param magnitudes: edge response gradient values of each pixel
        :param orientations: orientations of pixel like atan(Gx/Gy)
        :return: array of :class Box values
        """
        self.__cluster_edges(magnitudes, orientations)
        self.__prepare_data_structures(magnitudes)
        return self.__score_all_boxes()

    def __cluster_edges(self, magnitudes, orientations):
        self.__segment_ids = np.zeros(magnitudes.shape)
        self.__segment_count = 1
        (w, h) = magnitudes.shape
        for (x, y), mi in np.ndenumerate(magnitudes):
            is_border = x == 0 or y == 0 or x == w - 1 or y == h - 1
            # TODO: replace 0.1 with generator ctor param
            if is_border or mi <= 0.1:
                self.__segment_ids[x, y] = -1
        # groups initializing
        self.__generate_edge_groups(magnitudes, orientations)
        self.__merge_and_remove_small_segments(magnitudes, orientations)
        self.compact_magnitudes(magnitudes)

        seg_cnt_shape = (self.__segment_count, self.__segment_count)
        self.__segment_affinities = np.zeros(seg_cnt_shape, dtype=np.float32)
        self.__segment_affinities_idx = np.zeros(seg_cnt_shape, dtype=np.uint16)
        rad = 2
        for x in xrange(rad, w - rad):
            for y in xrange(rad, h - rad):
                s0 = self.__segment_ids[x, y]
                if s0 <= 0:
                    continue
                for dx in xrange(-rad, rad):
                    for dy in xrange(-rad, rad):
                        found = False
                        for i in xrange(0, self.__segment_affinities_idx[s0])

        """
         // compute segment affinities
  const int rad = 2;
  for( c=rad; c<w-rad; c++ ) for( r=rad; r<h-rad; r++ ) {
    int s0=_segIds.val(c,r); if( s0<=0 ) continue;
    for( cd=-rad; cd<=rad; cd++ ) for( rd=-rad; rd<=rad; rd++ ) {
      int s1=_segIds.val(c+cd,r+rd); if(s1<=s0) continue;
      bool found = false; for(i=0;i<_segAffIdx[s0].size();i++)
        if(_segAffIdx[s0][i] == s1) { found=true; break; }
      if( found ) continue;
      float o=atan2(meanY[s0]-meanY[s1],meanX[s0]-meanX[s1])+PI/2;
      float a=fabs(cos(meanO[s0]-o)*cos(meanO[s1]-o)); a=pow(a,_gamma);
      _segAff[s0].push_back(a); _segAffIdx[s0].push_back(s1);
      _segAff[s1].push_back(a); _segAffIdx[s1].push_back(s0);
    }
  }
        """

    def compact_magnitudes(self, magnitudes):
        (w, h) = magnitudes.shape
        np.resize(self.__segment_magnitudes, self.__segment_count)
        temp_map = np.zeros((self.__segment_count, self.__segment_count))
        self.__segment_count = 1
        for x in xrange(1, w - 1):
            for y in xrange(1, h - 1):
                j = self.__segment_ids[x, y]
                if j > 0:
                    self.__segment_magnitudes[j] += magnitudes[x, y]
        for i, m in enumerate(self.__segment_magnitudes):
            if m > 0:
                self.__segment_count += 1
                temp_map[i] = self.__segment_count
        for x in xrange(1, w - 1):
            for y in xrange(1, h - 1):
                j = self.__segment_ids[x, y]
                if j > 0:
                    self.__segment_ids[x, y] = temp_map[j]

    def __merge_and_remove_small_segments(self, magnitudes, orientations):
        (w, h) = magnitudes.shape
        self.__segment_magnitudes = np.zeros(self.__segment_count)
        for x in xrange(1, w - 1):
            for y in xrange(1, h - 1):
                j = self.__segment_ids[x, y]
                if j > 0:
                    self.__segment_magnitudes[j] += magnitudes[x, y]
        for x in xrange(1, w - 1):
            for y in xrange(1, h - 1):
                j = self.__segment_ids[x, y]
                # TODO: replace with parameter
                if j > 0 and self.__segment_magnitudes[j] <= 0.3:
                    self.__segment_ids[x, y] = 0
        j = 0
        finished = False
        while not finished:
            finished = True
            for x in xrange(1, w - 1):
                for y in xrange(1, h - 1):
                    if self.__segment_ids[x, y] != 0:
                        continue
                    o0 = orientations[x, y]
                    minv = 1000
                    for dx in xrange(-1, 1):
                        for dy in xrange(-1, 1):
                            if self.__segment_ids[x + dx, y + dy] <= 0:
                                continue

                            o1 = orientations[x + dx, y + dy]
                            v = math.fabs(o1 - o0) / math.pi
                            if v > .5:
                                v = 1 - v

                            if v < minv:
                                minv = v
                                j = self.__segment_ids[x + dx, y + dy]
                    self.__segment_ids[x, y] = j
                    if j > 0:
                        finished = False

    def __generate_edge_groups(self, magnitudes, orientations):
        (w, h) = magnitudes.shape
        for x in xrange(1, w - 1):
            for y in xrange(1, h - 1):
                # skip non edges
                if self.__segment_ids[x, y] != 0:
                    continue

                sum_cluster, x0, y0 = 0, x, y
                os = xs = ys = []
                # TODO: replace pi/2 with generator ctor param
                while sum_cluster != math.pi / 2:
                    self.__segment_ids[x0, y0] = self.__segment_count
                    o0 = orientations[x0, y0]
                    found = False
                    # lookup for 8 pixel around pixel(x, y)
                    for dx in xrange(-1, 1):
                        for dy in xrange(-1, 1):
                            y_near = y0 + dy
                            x_near = x0 + dx
                            if self.__segment_ids[x_near, y_near] != 0:
                                continue

                            for i, xss in enumerate(xs):
                                if xss == x_near and ys[i] == y_near:
                                    found = True
                                    break
                            if found:
                                continue

                            o1 = orientations[x_near, y_near]
                            o_new = math.fabs(o1 - o0) / math.pi
                            if o_new > .5:
                                o_new = 1 - o_new

                            os.append(o_new)
                            xs.append(x_near)
                            ys.append(y_near)

                    min_o = 1000
                    j = 0
                    for i, oss in enumerate(os):
                        if oss < min_o:
                            min_o = oss
                            x0 = xs[i]
                            y0 = ys[i]
                            j = i
                    sum_cluster += min_o
                    if min_o < 1000:
                        os[j] = 1000

                self.__segment_count += 1

    def __score_box(self, box):
        pass

    def __prepare_data_structures(self, magnitudes):
        """
        Initialize optimal data structures
        :param magnitudes: edge response gradient values of each pixel
        """
        pass

    def __score_all_boxes(self):
        pass
