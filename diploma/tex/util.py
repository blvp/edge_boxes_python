from collections import defaultdict


def clamp(val, _min, _max):
    return min(max(val, _min), _max)


# shortcut for defaultdict(float)
class Df(defaultdict):
    def __init__(self, **kwargs):
        super(Df, self).__init__(float, **kwargs)


class Di(defaultdict):
    def __init__(self, **kwargs):
        super(Di, self).__init__(int, **kwargs)


def gen_key(x, y):
    return x << 32 | y & 0xFFFFFFFFL


def find_min(xs):
    min_val = 1 << 32
    min_index = -1
    for i, x in enumerate(xs):
        if x <= min_val:
            min_val = x
            min_index = i
    return min_index, min_val
