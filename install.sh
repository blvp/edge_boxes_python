#!/usr/bin/env bash


if [[  ! -d "structured_forest" || ! -d "model" ]]; then
    echo "Creating directories"

    mkdir structured_forest
    mkdir model

else
    echo "Removing directories and files"

    rm -rf model
    rm -rf structured_forest
    rm -rf .pyxbld
    rm -f *.pyxbld
    rm -f *.cpp
    rm -f *.h
    rm -f *.pyx
fi

echo "Initializing"

git clone https://github.com/ArtanisCV/StructuredForests.git structured_forest

touch structured_forest/__init__.py

cp -R structured_forest/model model
cp -R structured_forest/_* ./

rm -f structured_forest/__init__.py

python structured_forest/StructuredForests.py

echo "Initializing ended"



